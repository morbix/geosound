//
//  AppDelegate.h
//  GeoSound
//
//  Created by HP Developer on 13/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSNumber *playlist;

@end
