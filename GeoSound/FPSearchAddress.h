//
//  FPSearchAdress.h
//  FindPets
//
//  Created by Henrique Morbin on 30/03/13.
//  Copyright (c) 2013 Henrique. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FPSearchAddressDelegate;

@interface FPSearchAddress : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate, NSXMLParserDelegate>
{
    NSMutableData *mutableData;
    BOOL isSearching;
    BOOL isLocation;
    BOOL isLat;
    BOOL isLng;
    
    NSString *latitude;
    NSString *longitude;
}

@property (strong, nonatomic) id <FPSearchAddressDelegate> delegate;

- (void) searchCoordinatesForAddress:(NSString *)inAddress;
@end


@protocol FPSearchAddressDelegate

@required
- (void)searchAddress:(FPSearchAddress *)delegate andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude;

@end
