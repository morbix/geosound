//
//  FPSearchAdress.m
//  FindPets
//
//  Created by Henrique Morbin on 30/03/13.
//  Copyright (c) 2013 Henrique. All rights reserved.
//

#import "FPSearchAddress.h"

@implementation FPSearchAddress

@synthesize delegate = _delegate;

- (void)searchCoordinatesForAddress:(NSString *)inAddress
{
    //NSLog(@"%@", inAddress);
    //Build the string to Query Google Maps.
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?address=%@&sensor=true",inAddress];
    
    //Replace Spaces with a '+' character.
    [urlString setString:[urlString stringByReplacingOccurrencesOfString:@" " withString:@"+"]];
    
    //Create NSURL string from a formate URL string.
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    
    isSearching = connection ? YES : NO;
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@",error.description);
    isSearching = NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"%@",response.description);
    
    if (mutableData) {
        mutableData = nil;
    }
    
    mutableData = [NSMutableData new];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [mutableData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"%@",mutableData.description);
    
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:mutableData];
    [parser setDelegate:self];
    [parser parse];
    
    isSearching = YES;
}

#pragma mark - NSXMLParseDelegate

-(void)parserDidStartDocument:(NSXMLParser *)parser
{
    latitude = @"0.0";
    longitude = @"0.0";
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    [self.delegate searchAddress:self andLatitude:latitude andLongitude:longitude];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"location"]) {
        isLocation = YES;
    }
    
    if (isLocation) {
        if ([elementName isEqualToString:@"lat"]) {
            isLat = YES;
        }
        
        if ([elementName isEqualToString:@"lng"]) {
            isLng = YES;
        }
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (isLat) {
        //NSLog(@"Lat: %@", string);
        latitude = string;
    }
    
    if (isLng) {
        //NSLog(@"Lng: %@", string);
        longitude = string;
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"location"]) {
        isLocation = NO;
    }
    
    if (isLocation) {
        if ([elementName isEqualToString:@"lat"]) {
            isLat = NO;
        }
        
        if ([elementName isEqualToString:@"lng"]) {
            isLng = NO;
        }
    }
}


@end
