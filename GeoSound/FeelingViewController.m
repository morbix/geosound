//
//  FeelingViewController.m
//  GeoSound
//
//  Created by HP Developer on 23/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "FeelingViewController.h"
#import "GeoSound.h"


@interface FeelingViewController ()

@end

@implementation FeelingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Home";
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPlaylistTouched:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    [[GeoSound shared] setPlaylistSelected:(int)button.tag];
    
    [self performSegueWithIdentifier:@"locationViewController" sender:nil];
}
@end
