//
//  GSPlayer.h
//  GeoSound
//
//  Created by HP Developer on 15/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface GSPlayer : AVAudioPlayer <AVAudioPlayerDelegate>

@property (nonatomic, assign) BOOL UPPING;

-(id)initWithTrack:(NSString *)track__;

@end
