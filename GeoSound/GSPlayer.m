//
//  GSPlayer.m
//  GeoSound
//
//  Created by HP Developer on 15/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "GSPlayer.h"

@implementation GSPlayer 

-(id)initWithTrack:(NSString *)track__
{
    NSURL *url = [[NSBundle mainBundle] URLForResource:track__
                                         withExtension:@"mp3"];
    //NSURL *url = [[NSBundle mainBundle] URLForResource:@"playlist1_track1" withExtension:@"mp3"];
    //NSURL *url = [[NSBundle mainBundle] URLForResource:@"background-music-aac" withExtension:@"wav"];
    
    NSError *error;
    self = [[GSPlayer alloc] initWithContentsOfURL:url
                                             error:&error];
    if (self) {
        [self setDelegate:self];
        [self prepareToPlay];
        [self setNumberOfLoops:-1];
        [self setMeteringEnabled:YES];
        [self setVolume:0.0f];
        self.UPPING = FALSE;
        
        NSError *sessionError = nil;
        [[AVAudioSession sharedInstance] setDelegate:self];
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
        
        [NSTimer scheduledTimerWithTimeInterval:0.8
                                         target:self
                                       selector:@selector(onTimer)
                                       userInfo:nil
                                        repeats:YES];
    }
    
    return self;
}

- (void)onTimer
{
    if (self.UPPING) {
        if (self.volume < 1.0f) {
            self.volume = self.volume + 0.1f;
        }
    }else{
        if ((self.volume-0.1f) >= 0.0f) {
            self.volume = self.volume - 0.1f;
        }
    }
    //NSLog(@"%f", self.volume);
}
@end
