//
//  GeoSound.h
//  GeoSound
//
//  Created by HP Developer on 23/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeoSound : NSObject

@property (nonatomic, assign) int playlistSelected;
@property (nonatomic, assign) float latitudeReference;
@property (nonatomic, assign) float longitudeReference;
@property (nonatomic, strong) NSMutableArray *arrayLocations;

+ (id)shared;

- (void)saveCachedLocations;
@end
