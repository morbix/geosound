//
//  GeoSound.m
//  GeoSound
//
//  Created by HP Developer on 23/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "GeoSound.h"

@implementation GeoSound

+ (id)shared
{
    static GeoSound *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [GeoSound new];
        
        manager.arrayLocations = [NSMutableArray new];
        
        [manager loadCachedLocations];
    });
    
    return manager;
}

- (void)loadCachedLocations
{
    NSArray *cachedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"cachedLocations"];
    
    if (cachedArray) {
        for (NSDictionary *dict in cachedArray) {
            [self.arrayLocations addObject:dict];
        }
    }
}

- (void)saveCachedLocations
{
    NSArray *array = [NSArray arrayWithArray:self.arrayLocations];
    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"cachedLocations"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
@end
