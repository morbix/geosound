//
//  Location.h
//  GeoSound
//
//  Created by HP Developer on 17/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#define GSNotificationNewLocation @"com.matanay.GeoSound.notification.newlocation"
#define GSNotificationError @"com.matanay.GeoSound.notification.error"

@interface Location : CLLocationManager <CLLocationManagerDelegate>

+ (id)shared;
@end
