//
//  Location.m
//  GeoSound
//
//  Created by HP Developer on 17/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "Location.h"
#import "GeoSound.h"

@implementation Location

+ (id)shared
{
    static Location *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[Location alloc] init];
    });
    
    return manager;
}
- (id)init
{
    self = [super init];
    if (self) {
        [self setDesiredAccuracy:kCLLocationAccuracyBest];
        self.delegate = self;
    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@", error.description);
    [[NSNotificationCenter defaultCenter] postNotificationName:GSNotificationError
                                                        object:nil
                                                      userInfo:@{@"error": error}];
}
- (void)locationManager:(CLLocationManager *)manager
	 didUpdateLocations:(NSArray *)locations
{
    if (locations) {
        if (locations.count > 0) {
            //NSLog(@"\n%@",locations.description);
            CLLocation *location = locations.lastObject;
            
            
            //self.textInformation.text = location.description;
            //self.labelDate.text = [NSString stringWithFormat:@"%f,%f", location.horizontalAccuracy, location.verticalAccuracy];
            //self.labelLocation.text = [NSString stringWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
            
            
            /*Location functions here*/
			//Rua Guilhermina Guinle - Botafogo
			//-22.950806,-43.185561
			//-22.95254398, -43.18777520
			
			//Porto Alegre
			//-30.034659,-51.217661
			
			//Canoas
			//-29.9391974,-51.1761886
            CLLocation *locationMonitored = [[CLLocation alloc] initWithLatitude:[[GeoSound shared] latitudeReference]
                                                                       longitude:[[GeoSound shared] longitudeReference]];
            
            float distancia = [location distanceFromLocation:locationMonitored];//em metros
            
            [[NSNotificationCenter defaultCenter] postNotificationName:GSNotificationNewLocation
                                                                object:nil
                                                              userInfo:@{@"distancia": [NSNumber numberWithFloat:distancia], @"data" : location.timestamp}];
            
            /*if (self.mySegmented.selectedSegmentIndex == 0) {
                self.labelDistancia.text = [NSString stringWithFormat:@"%.4fM", distancia];
            }else{
                self.labelDistancia.text = [NSString stringWithFormat:@"%.3fKM", distancia/1000];
            }*/
            
            
        }
    }
}
@end
