//
//  LocationViewController.m
//  GeoSound
//
//  Created by HP Developer on 23/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "LocationViewController.h"
#import "GeoSound.h"
#import "UIImageView+LBBlurredImage.h"

@interface LocationViewController ()

@end

@implementation LocationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSArray *arrayImages = @[@"_PrecisoMeEmpolgar.jpg", @"_SoQueroCurtir.jpg", @"_PrecisoFicarCalmo.jpg"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.tableView.frame];
    
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setImageToBlur:[UIImage imageNamed:[arrayImages objectAtIndex:[[GeoSound shared] playlistSelected]]]
                   blurRadius:8.0f
              completionBlock:nil];
    
    [self.tableView setBackgroundView:imageView];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[GeoSound shared] arrayLocations] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *dict = [[[GeoSound shared] arrayLocations] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = dict[@"title"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%f,%f", [(NSNumber *)dict[@"latitude"] floatValue], [(NSNumber *)dict[@"longitude"] floatValue] ];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [[[GeoSound shared] arrayLocations] objectAtIndex:indexPath.row];
    
    [[GeoSound shared] setLatitudeReference:[(NSNumber *)dict[@"latitude"] floatValue]];
    [[GeoSound shared] setLongitudeReference:[(NSNumber *)dict[@"longitude"] floatValue]];
    
    UIViewController *tela = [self.storyboard instantiateViewControllerWithIdentifier:@"viewController"];
    tela.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    UIViewController *root = [[self.navigationController viewControllers] objectAtIndex:0];
    
    [root.navigationController popToRootViewControllerAnimated:NO];
    [root presentViewController:tela animated:YES completion:nil];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[[GeoSound shared] arrayLocations] removeObjectAtIndex:indexPath.row];
        [[GeoSound shared] saveCachedLocations];
        
        [tableView reloadData];
    }
}
@end
