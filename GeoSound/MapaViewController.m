//
//  MapaViewController.m
//  GeoSound
//
//  Created by HP Developer on 23/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "MapaViewController.h"
#import "FPSearchAddress.h"
#import <MapKit/MapKit.h>
#import "REMarker.h"
#import "GeoSound.h"


@interface MapaViewController () <UITextFieldDelegate, FPSearchAddressDelegate, MKMapViewDelegate>
{
    FPSearchAddress *searchAddress;
    REMarker *marker;
    __weak IBOutlet UIImageView *imagePin;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapa;
@property (weak, nonatomic) IBOutlet UIImageView *imageSombra;
@property (weak, nonatomic) IBOutlet UITextField *textEndereco;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@end

@implementation MapaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.mapa.delegate = self;
    
    imagePin.hidden = YES;
    
    marker = [[REMarker alloc] init];
    marker.coordinate = CLLocationCoordinate2DMake(0.0f,0.0f);
    [self.mapa addAnnotation:marker];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.textEndereco becomeFirstResponder];
    
    marker.coordinate = self.mapa.centerCoordinate;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (searchAddress) {
        searchAddress = nil;
    }
    
    searchAddress = [FPSearchAddress new];
    [searchAddress setDelegate:self];
    [searchAddress searchCoordinatesForAddress:[textField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    return YES;
}

- (IBAction)barButtonRightTouched:(id)sender
{
    NSDictionary *dict = [[NSDictionary alloc] initWithObjects:@[[NSNumber numberWithFloat:marker.coordinate.latitude], [NSNumber numberWithFloat:marker.coordinate.longitude], self.textEndereco.text]
                                                       forKeys:@[@"latitude", @"longitude", @"title"]];
    
    [[[GeoSound shared] arrayLocations] addObject:dict];
    [[GeoSound shared] setLatitudeReference:marker.coordinate.latitude];
    [[GeoSound shared] setLongitudeReference:marker.coordinate.longitude];
    [[GeoSound shared] saveCachedLocations];
    
    UIViewController *tela = [self.storyboard instantiateViewControllerWithIdentifier:@"viewController"];
    tela.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    UIViewController *root = [[self.navigationController viewControllers] objectAtIndex:0];
    
    [root.navigationController popToRootViewControllerAnimated:NO];
    [root presentViewController:tela animated:YES completion:nil];
    
}

#pragma mark - FPSearchAddressDelegate
-(void)searchAddress:(FPSearchAddress *)delegate andLatitude:(NSString *)latitude andLongitude:(NSString *)longitude
{
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(latitude.floatValue, longitude.floatValue);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.059863, 0.059863);
    span = MKCoordinateSpanMake(0.01f, 0.01f);
    
    MKCoordinateRegion region = MKCoordinateRegionMake(location, span);
    
    [self.mapa setRegion:region animated:YES];
}

#pragma mark - MKMapViewDelegate
-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    imagePin.hidden = NO;
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    imagePin.hidden = YES;
    marker.coordinate = self.mapa.centerCoordinate;
}
@end
