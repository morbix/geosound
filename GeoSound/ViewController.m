//
//  ViewController.m
//  GeoSound
//
//  Created by HP Developer on 13/02/14.
//  Copyright (c) 2014 Matanay. All rights reserved.
//

#import "ViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "VisualizerView.h"
#import "GSPlayer.h"
#import "Location.h"
#import "GeoSound.h"

@interface ViewController ()
{
    VisualizerView *visualizer;
    NSArray *arrayPlayers;
    NSMutableArray *arrayTimes;
    GSPlayer *player1;
    GSPlayer *player2;
    GSPlayer *player3;
    GSPlayer *player4;
    NSNumber *time1;
    NSNumber *time2;
    NSNumber *time3;
    NSNumber *time4;
}
@property (weak, nonatomic) IBOutlet UILabel *labelData;
@property (weak, nonatomic) IBOutlet UILabel *labelDistancia;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *btnPLay;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self.btnPLay.layer setCornerRadius:40];
    
    time1 = @0.0f;
    time2 = @0.0f;
    time3 = @0.0f;
    time4 = @0.0f;
    
    int indexPlaylist = [[GeoSound shared] playlistSelected];
    
    player1 = [[GSPlayer alloc] initWithTrack:[NSString stringWithFormat:@"playlist%d_track1_64kbps", indexPlaylist+1]];
    player2 = [[GSPlayer alloc] initWithTrack:[NSString stringWithFormat:@"playlist%d_track2_64kbps", indexPlaylist+1]];
    player3 = [[GSPlayer alloc] initWithTrack:[NSString stringWithFormat:@"playlist%d_track3_64kbps", indexPlaylist+1]];
    player4 = [[GSPlayer alloc] initWithTrack:[NSString stringWithFormat:@"playlist%d_track4_64kbps", indexPlaylist+1]];
    
    arrayPlayers = [NSArray arrayWithObjects:player1, player2, player3, player4, nil];
    arrayTimes   = [NSMutableArray new];
    [arrayTimes addObjectsFromArray:@[time1, time2, time3, time4]];
    
    UIView *view = (UIView *)[self.view viewWithTag:10];
    visualizer = [[VisualizerView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    visualizer.audioPlayer = player1;
    
    [view addSubview:visualizer];
    
    //for (GSPlayer *player in arrayPlayers) {
        //[player setVolume:0.0f];
    //}
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callbackNewLocation:)
                                                 name:GSNotificationNewLocation
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callbackError:)
                                                 name:GSNotificationError
                                               object:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions
- (IBAction)btnPlayTouched:(id)sender
{
    if (self.btnPLay.tag == 0) {
        self.btnPLay.tag = 1;
        for (GSPlayer *player in arrayPlayers) {
            [player play];
            [self.btnPLay setTitle:@"Pause" forState:UIControlStateNormal];
            [self.labelDistancia setText:@"searching..."];
            [self.labelData      setText:@""];
            [self pageControlChanged:nil];
            [[Location shared] startUpdatingLocation];
        }
    }else{
        self.btnPLay.tag = 0;
        for (GSPlayer *player in arrayPlayers) {
            [player pause];
            [self.btnPLay setTitle:@"Play" forState:UIControlStateNormal];
            [self.labelDistancia setText:@""];
            [self.labelData      setText:@""];
            [[Location shared] stopUpdatingLocation];
        }
    }
}

- (IBAction)btnBackTouched:(id)sender
{
    for (GSPlayer *player in arrayPlayers) {
        [player stop];
    }
    
    [[Location shared] stopUpdatingLocation];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)switchValueChanged:(UISwitch *)sender
{
    self.pageControl.userInteractionEnabled = !sender.on;
}

- (IBAction)pageControlChanged:(id)sender
{
    AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
    
    for (int i = 0; i < 4; i++) {
        if (i == self.pageControl.currentPage) {
            GSPlayer *player = [arrayPlayers objectAtIndex:i];
            NSNumber *time   = [arrayTimes objectAtIndex:i];
            player.UPPING = YES;
            [player setCurrentTime:time.floatValue];
            //[player setVolume:1.0f];
        }else{
            GSPlayer *player = [arrayPlayers objectAtIndex:i];
            player.UPPING = NO;
            [arrayTimes replaceObjectAtIndex:i withObject:[NSNumber numberWithFloat:player.currentTime]];
            //[player setVolume:0.0f];
        }
    }
    
    
    switch (self.pageControl.currentPage) {
        case 0:
        {
            [visualizer updateFactor:6];
        }
            break;
        case 1:
        {
            [visualizer updateFactor:3];
        }
            break;
        case 2:
        {
            [visualizer updateFactor:1];
        }
            break;
        case 3:
        {
            [visualizer updateFactor:0.5];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - GSNotification
- (void)callbackError:(NSNotification *)notification
{
    [self.labelDistancia setText:@"couldn't get user location"];
    [self.labelData      setText:@""];
}

- (void)callbackNewLocation:(NSNotification *)notification
{
    UISwitch *mySwitch = (UISwitch *)[self.view viewWithTag:990];
    if (!mySwitch.on) {
        return;
    }
    float distancia = [(NSNumber *)[notification.userInfo objectForKey:@"distancia"] floatValue];
    //NSDate *date = (NSDate *)[notification.userInfo objectForKey:@"data"];
    
    
    [self.labelDistancia setText:[NSString stringWithFormat:@"%.2fm", distancia]];
    //[self.labelData      setText:[NSString stringWithFormat:@"%@", date]];
    
    //NSLog(@"%@ - %@",self.labelDistancia.text, self.labelData.text);
    
    int newIndex;
    if (distancia > 180) {
        newIndex = 3;//default
    }else if (distancia > 100) {
        newIndex = 2;//100
    }else if (distancia > 30) {
        newIndex = 1;//30
    }else{
        newIndex = 0;//0
    }
    
    if (newIndex != self.pageControl.currentPage) {
        [self.pageControl setCurrentPage:newIndex];
        [self pageControlChanged:self.pageControl];
    }
    
}
@end
