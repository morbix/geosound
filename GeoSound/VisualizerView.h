//
//  VisualizerView.h
//  iPodVisualizer
//
//  Created by HP Developer on 13/02/14.
//  Copyright (c) 2014 Xinrong Guo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VisualizerView : UIView

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

- (void)updateFactor:(float)newFactor;

@end
