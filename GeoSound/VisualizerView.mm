#import "VisualizerView.h"
#import <QuartzCore/QuartzCore.h>
#import "MeterTable.h"

@implementation VisualizerView {
    CAEmitterLayer *emitterLayer;
    MeterTable meterTable;
    float factor;
}

+ (Class)layerClass {
    return [CAEmitterLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        emitterLayer = (CAEmitterLayer *)self.layer;
        
        // 2
        CGRect bounds = [[UIScreen mainScreen] bounds];
        CGFloat width = MIN(bounds.size.width, bounds.size.height);
        CGFloat height = MAX(bounds.size.width, bounds.size.height);
        emitterLayer.emitterPosition = CGPointMake(width/2, height/2);
        emitterLayer.emitterSize = CGSizeMake(/*width-*/280, 280);
        emitterLayer.emitterShape = kCAEmitterLayerCircle;
        emitterLayer.emitterMode  = kCAEmitterLayerVolume;
        emitterLayer.renderMode = kCAEmitterLayerAdditive;
        
        // 3
        CAEmitterCell *cell = [CAEmitterCell emitterCell];
        cell.name = @"cell";
        CAEmitterCell *childCell = [CAEmitterCell emitterCell];
        childCell.name = @"childCell";
        childCell.lifetime = 1.0f / 60.0f;
        childCell.birthRate = 60.0f;
        childCell.velocity = 0.0f;
        
        childCell.contents = (id)[[UIImage imageNamed:@"particleTexture.png"] CGImage];
        
        cell.emitterCells = @[childCell];
        
        cell.color = [[UIColor colorWithRed:1.0f green:0.53f blue:0.0f alpha:0.8f] CGColor];
        
        // 6
        cell.scale = 0.5f;
        cell.scaleRange = 0.5f;
        
        // 7
        cell.lifetime = 2.0f;
        cell.lifetimeRange = .25f;
        cell.birthRate = 80;
        
        // 8
        cell.velocity = 100.0f;
        cell.velocityRange = 300.0f;
        cell.emissionRange = M_PI * 2;
        //cell.spin = M_PI;
        //cell.spinRange = 0.5f;
        
        // 9
        emitterLayer.emitterCells = @[cell];
        factor = 0.5;
        CADisplayLink *dpLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update)];
        [dpLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)update
{
    // 1
    float scale = 0.1;
    if (_audioPlayer.playing )
    {
        // 2
        [_audioPlayer updateMeters];
        
        // 3
        float power = 0.0f;
        for (int i = 0; i < [_audioPlayer numberOfChannels]; i++) {
            power += [_audioPlayer averagePowerForChannel:i];
        }
        power /= [_audioPlayer numberOfChannels];
        
        // 4
        float level = meterTable.ValueAt(power);
        scale = level * factor;
        
        float r = 1 + arc4random_uniform(255);
        float g = 1 + arc4random_uniform(255);
        float b = 1 + arc4random_uniform(255);
        CGColorRef newColor = [[UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0f] CGColor];
        [(CAEmitterCell *)emitterLayer.emitterCells[0] setColor:newColor];
    }else{
        CGColorRef oldColor = [[UIColor colorWithRed:1.0f green:0.53f blue:0.0f alpha:0.8f] CGColor];
        [(CAEmitterCell *)emitterLayer.emitterCells[0] setColor:oldColor];
    }
    
    [emitterLayer setValue:@(scale) forKeyPath:@"emitterCells.cell.emitterCells.childCell.scale"];
}

- (void)updateFactor:(float)newFactor
{
    factor = newFactor;
    
    if (newFactor == 0.5) {
        emitterLayer.emitterSize = CGSizeMake(10, 10);
    }else if (newFactor == 1) {
        emitterLayer.emitterSize = CGSizeMake(20, 20);
    }else if (newFactor == 2) {
        emitterLayer.emitterSize = CGSizeMake(40, 40);
    }else if (newFactor == 3) {
        emitterLayer.emitterSize = CGSizeMake(80, 80);
    }else if (newFactor == 4) {
        emitterLayer.emitterSize = CGSizeMake(120, 120);
    }else if (newFactor == 5) {
        emitterLayer.emitterSize = CGSizeMake(200, 200);
    }else if (newFactor == 6) {
        emitterLayer.emitterSize = CGSizeMake(280, 280);
    }
}
@end